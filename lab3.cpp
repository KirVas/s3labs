#define _CRT_SECURE_NO_WARNINGS 
#include <iostream>
#include <cstring>

using namespace std;

class Node
{
	char * name = NULL;
	Node * left;
	Node * right;
public:
	Node(char * name, Node * left, Node * right) : name(name), left(left), right(right) {}
	Node(char * name) : name(name), left(NULL), right(NULL) {}
	Node()
	{
		name = "defaultname";
		left = NULL;
		right = NULL;
	}
	Node(const Node& n)
	{
		char * myName = new char[50];
		myName = strcpy(myName, n.name);
		left = n.left;
		right = n.right;
	}
	~Node()
	{
		if (left != NULL) left->DelTree();
		if (right != NULL) right->DelTree();
		delete[] name;
	}
	void Init(char * myName)
	{
		left = NULL;
		right = NULL;
		name = myName;
	}
	void AddNode(char * name)
	{
		Node * search = this;
		Node *toAdd = new Node(name, NULL, NULL);
		while (true)
		{
			if (strcmp(search->name, name) > 0)
			{
				if (search->left != NULL) search = search->left;
				else
				{
					search->left = toAdd;
					return;
				}
			}
			else
			{
				if (search->right != NULL) search = search->right;
				else
				{
					search->right = toAdd;
					return;
				}
			}
		}
		return;
	}
	void DelTree()
	{
		delete this;
		return;
	}

	void PrintTree()
	{
		if (left != NULL) left->PrintTree();
		cout << name << endl;
		if (right != NULL) right->PrintTree();
		return;
	}
	char * GetName()
	{
		return name;
	}
};



int main()
{
	char * name = new char[10];
	cin >> name;
	Node * myTree = new Node(name);
	for (int i = 0; i < 3; i++)
	{
		char * elname = new char[10];
		cin >> elname;
		myTree->AddNode(elname);
	}
	myTree->PrintTree();
	myTree->DelTree();
	//system("pause");
	return 0;
}




