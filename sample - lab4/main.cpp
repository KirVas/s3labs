﻿#define _CRT_SECURE_NO_WARNINGS  // read as "allow me to shoot myself in the foot"
#include <iostream>
#include <cstring>

using namespace std;

class String
{
	/*
		Chinese-quality clone of STL's std::string class
	*/

public:
	String() {}

	String(const char *str_to_copy) 
	{
		this->len = strlen(str_to_copy); // will crash if no null-byte in str_to_copy
		if (this->len == 0) return;
		
		this->str = new char[this->len + 1];
		strcpy(this->str, str_to_copy);
		// strcpy also copies null-byte
	}

	String(const String &another)
	{
		if (another.str == nullptr) return;
		this->str = new char[another.len + 1];
		strcpy(this->str, another.str);
		this->len = another.len;
	}

	/*
		We can also make move constructor:
		String(String &&another)
		{
			this->len = another.len;
			this->str = another.str;
		}
		this will be extremely cool and fast,
		but I bet that you can't use move-semantics
		yet
	*/

	~String()
	{
		if (this->str) delete[] this->str;
	}

	bool operator==(const String &another) const
	{
		return (another.str == this->str) ||
			(another.str != nullptr && this->str != nullptr &&
				strcmp(this->str, another.str) == 0);
	}

	bool operator<(const String &another) const
	{ 
		return (this->str == nullptr && another.str != nullptr) ||
			(this->str != nullptr && another.str != nullptr && 
				strcmp(this->str, another.str) == -1);
	}

	bool operator>(const String &another) const
	{
		return (another.str == nullptr && this->str != nullptr) ||
			(this->str != nullptr && another.str != nullptr &&
				strcmp(this->str, another.str) == 1);
	}

	String &operator=(String another)
	{
		// we take by value => we already have a copy of this string
		// (our copy constructor was called implicitly)
		if (this->str) delete[] this->str;
		this->str = another.str;
		this->len = another.len;
		another.str = nullptr; // otherwise destructor would kill our string
		return *this;
	}

	char operator[](const unsigned int index) const
	{
		return this->str[index];  
		// will crash if (index > len + 1) or this->str == nullptr
	}

	String &operator+=(String &another)
	{
		if (another.str == nullptr) return *this;
		if (this->str == nullptr) return *this = another;

		char *new_str = new char[this->len + another.len + 1];

		strcpy(new_str, this->str);
		strcpy(new_str + this->len, another.str);
		this->len += another.len;

		delete[] this->str;
		this->str = new_str;

		return *this;
	}

	String operator+(String &another) const // extra junk
	{
		if (this->str == nullptr) return another;
		if (another.str == nullptr) return *this;

		String new_string;        // ~zero overhead

		new_string.len = this->len + another.len;
		new_string.str = new char[new_string.len + 1];

		strcpy(new_string.str, this->str);
		strcpy(new_string.str + this->len, another.str);

		return new_string;
	}

	String operator+(const char *c_str)
	{
		String new_string(c_str); 
		return *this + new_string;
	}

	String &operator+=(const char *c_str)
	{
		String new_string(c_str);
		*this += new_string;
		return *this;
	}

	void clear()
	{
		if (this->str)
		{
			delete[] this->str;
			this->str = nullptr;
			this->len = 0;
		}
	}

	unsigned int length() const { return this->len; }
	const char * data() const { return this->str; }
	bool empty() const { return this->str == nullptr; }

	friend istream &operator>>(istream &ist, String &str)
	{
		const unsigned int BUF_SIZE = 4095; 
		// not 4096 because I need to leave null-byte at the end
		// otherwise String will crash at str += buf
		char *buf = new char[BUF_SIZE + 1];
		memset(buf, '\0', BUF_SIZE + 1);
		unsigned int counter = 0;

		str.clear();

		while (true)
		{
			while (counter < BUF_SIZE && !ist.eof())
			{
				char ch = ist.get();
				if (isspace(ch)) break;
				buf[counter] = ch;
				++counter;
			}
			str += buf;
			if (counter == BUF_SIZE)
			{
				memset(buf, '\0', BUF_SIZE);  // no need in +1 to BUF_SIZE
				counter = 0;
				continue;
			}
			else break;
		}

		delete[] buf;
		return ist;
	}

	friend ostream &operator<<(ostream &ost, const String &str)
	{
		return ost << str.data();
	}

private:
	char *str = nullptr;
	unsigned int len = 0;
};

class Node
{
	String name = "defaultstring";
	Node * left = NULL;
	Node * right = NULL;

public:

	Node(const String &name, Node * left, Node * right) : name(name), left(left), right(right) {}
	Node(const String &name) : name(name), left(nullptr), right(nullptr) {}
	Node() {}

	~Node()
	{
		// delete will call destructors
		// it's the reason there is operator 'delete' in C++
		// and not just a library function 'free()' like in C
		if (left) delete[] left;
		if (right) delete[] right;
	}

	Node(const Node& n)
	{
		name = n.name;
		left = n.left;
		right = n.right;
	}

	Node& operator=(const Node& n)
	{
		if (this != &n)
		{
			name = n.name;
		}
	}

	Node& operator<<(const char* myName)
	{
		Node * toAdd = new Node(myName);
		left = toAdd;
		return *this;
	}
	Node& operator>(const char* myName)
	{
		Node * toAdd = new Node(myName);
		right = toAdd;
		return *this;
	}
	Node& operator[](const char *myName)
	{
		Node * search = this;
		while (true)
		{
			if (search->name == myName) return *search;
			// notice how I changed the compare from < to > because of
			// myName being a c-string
			if (search->name > myName)  
			{
				if (left != NULL) search = left; else return *this;
			}
			else
			{
				if (right != NULL) search = right; else return *this;
			}
		}

	}
	/*void DelNode()
	{
		delete this;  // delete this
		return;
	}*/

	void AddNode(const char * name)
	{
		Node * search = this;
		Node *toAdd = new Node(name);  // you can use constructors with new
		while (true)
		{
			if (search->name < name)  // also changed > to < here
			{
				if (search->right != NULL) search = search->right;
				else
				{
					search->right = toAdd;
					return;
				}
			}
			else
			{
				if (search->left != NULL) search = search->left;
				else
				{
					search->left = toAdd;
					return;
				}
			}
		}
		return;
	}

	void PrintTree()
	{
		if (left != NULL) left->PrintTree();
		cout << name << endl;
		if (right != NULL) right->PrintTree();
		return;
	}
	String GetName()
	{
		return name;
	}
};



int main()
{
	String name;
	cin >> name;
	Node myTree(name);
	for (int i = 0; i < 3; i++)
	{
		String elname;
		cin >> elname;
		myTree.AddNode(elname.data());
	}
	cout << "================================" << endl;
	cout << myTree["abasa"].GetName() << endl;
	cout << "================================" << endl;
	myTree.PrintTree();
	// myTree will destruct itself at the end of the block
	system("pause");  //								|
	return 0;  //										|
} // right here <---------------------------------------/