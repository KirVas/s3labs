#pragma once
#include "MathException.h"
class UnderflowException : public MathException
{
public:
	UnderflowException(char * name_op, long v1, long v2) : MathException(name_op, v1, v2) {};
	~UnderflowException() {};

	char * Message()
	{

		char * message = new char[100];
		message[0] = '\0';
		strcat(message, "Exception! ");
		strcat(message, op_name);
		strcat(message, " result is too little!");
		return message;
	}
};

