#pragma once
#define _CRT_SECURE_NO_WARNINGS

#include <cstring>
#include <cstdlib>

class MathException
{
protected:
	char op_name[20];
	long val1;
	long val2;
public:
	MathException(char * name_op, long v1, long v2)
	{
		val1 = v1;
		val2 = v2;
		strcpy(op_name, name_op);
	}
	virtual char * Message() = 0;
};

