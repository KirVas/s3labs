#pragma once
#include "MathException.h"
class OverflowException : public MathException
{
public:
	OverflowException(char * name_op, long v1, long v2) : MathException(name_op, v1, v2) {};
	~OverflowException() {};

	char * Message()
	{

		char * message = new char[100];
		message[0] = '\0';
		strcat(message, "Exception! ");
		strcat(message, op_name);
		strcat(message, " result is too big!");
		return message;
	}
};

