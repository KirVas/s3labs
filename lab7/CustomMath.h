#define _CRT_SECURE_NO_WARNINGS
#pragma once

#include "ZdivideException.h"
#include "OverflowException.h"
#include "UnderflowException.h"
#include <climits>

class CustomMath
{
public:
	CustomMath() {};
	~CustomMath() {};
	long Add(long a, long b)
	{
		if (a > 0 && b > 0 && a > LONG_MAX - b) throw OverflowException("Add", a, b);
		if (a < 0 && b < 0 && a < LONG_MIN - b) throw UnderflowException("Add", a, b);
		return a + b;
	}
	long Sub(long a, long b)
	{
		if (a > 0 && b < 0 && ((-1 * b) > (LONG_MAX - a))) throw OverflowException("Sub", a, b);
		if (a < 0 && b > 0 && ((-1 * b) < (LONG_MIN - a))) throw UnderflowException("Sub", a, b);
		return a - b;
	}
	long Mul(long a, long b)
	{
		if (a == 0 || b == 0) return 0;
		if (((a > 0 && b > 0) || (a < 0 && b < 0)) && (b > (LONG_MAX / a))) throw OverflowException("Mul", a, b);
		if (((a > 0 && b < 0) || (a < 0 && b > 0)) && (LONG_MAX/Max(a,b) < (Min(a,b) * -1))) throw UnderflowException("Mul", a, b);
		return a * b;
	}
	long Div(long a, long b)
	{
		if (b == 0) throw ZdivideException("Div", a, b);
		return a / b;
	};
	long Mod(long a, long b)
	{
		if (b == 0) throw ZdivideException("Mod", a, b);
		return a % b;
	};

	long Max(long a, long b)
	{
		if (a > b) return a;
			else return b;
	}

	long Min(long a, long b)
	{
		if (a < b) return a;
		else return b;
	}
};

