#define _CRT_SECURE_NO_WARNINGS 
#include <iostream>
#include <cstring>
#include "CustomMath.h"
#include <climits>


using namespace std;

int main()
{
	CustomMath cm;



	try
	{
		cout << cm.Div(10, 5) << endl;
	}
	catch (ZdivideException ex)
	{
		cout << ex.Message();
	}
	try
	{
		cout << cm.Div(10, 0);
	}
	catch (ZdivideException ex)
	{
		cout << ex.Message() << endl;
	}





	try
	{
		cout << cm.Add(10, 5) << endl;
	}
	catch (OverflowException ex)
	{
		cout << ex.Message();
	}
	try
	{
		cout << cm.Add(10, LONG_MAX - 5);
	}
	catch (OverflowException ex)
	{
		cout << ex.Message() << endl;
	}





	try
	{
		cout << cm.Sub(10, 5) << endl;
	}
	catch (OverflowException ex)
	{
		cout << ex.Message();
	}
	try
	{
		cout << cm.Sub(10, LONG_MIN + 5);
	}
	catch (OverflowException ex)
	{
		cout << ex.Message() << endl;
	}
	try
	{
		cout << cm.Sub(-10, LONG_MAX - 5);
	}
	catch (UnderflowException ex)
	{
		cout << ex.Message() << endl;
	}


	try
	{
		cout << cm.Mod(10, 5) << endl;
	}
	catch (ZdivideException ex)
	{
		cout << ex.Message();
	}
	try
	{
		cout << cm.Mod(10, 0);
	}
	catch (ZdivideException ex)
	{
		cout << ex.Message() << endl;
	}


	try
	{
		cout << cm.Mul(10, 5) << endl;
	}
	catch (OverflowException ex)
	{
		cout << ex.Message();
	}
	try
	{
		cout << cm.Mul(LONG_MAX - 5, -15);
	}
	catch (OverflowException ex)
	{
		cout << ex.Message() << endl;
	}
	catch (UnderflowException ex)
	{
		cout << ex.Message() << endl;
	}





	// system("pause");
	return 0;
}