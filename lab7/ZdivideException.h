#pragma once
#define _CRT_SECURE_NO_WARNINGS
#include "MathException.h"
#include <stdlib.h>
#include <stdio.h>

class ZdivideException : public MathException
{
public:
	ZdivideException(char * name_op, long v1, long v2) : MathException(name_op, v1, v2) {};
	~ZdivideException() {};

	char * Message()
	{
		
		char * message = new char[100];
		message[0] = '\0';
		char * strval1 = new char[10];
		snprintf(strval1, sizeof(strval1), "%d", val1);
		strcat(message, "Exception! Can't divide ");
		strcat(message, strval1);
		strcat(message, " by zero!");
		return message;
	}

};

