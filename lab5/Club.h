#pragma once

#include <iostream>
#include "Weapon.h"
#include <string.h>

using namespace std;

class Club : public Weapon
{
public:

	Club(int bp, int pp, string name) : Weapon(bp, pp, name) {}

	int Strike()
	{
		int damage = rnd(blow_power);
		blow_power -= damage / 10;
		cout << "Your strike made " << damage << " damage" << endl;
		return damage;
	}
	int Protect(int damage)
	{
		int def = rnd(protect_power);
		protect_power = protect_power - def / 10;
		def = damage - def;
		def = (def > 0 ? def : 0);
		cout << "You've blocked the strike and took " << def << " damage" << endl;
		return def;
	}

protected:

private:
};
