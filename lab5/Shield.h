#pragma once

#include <iostream>
#include "Weapon.h"
#include <string.h>

using namespace std;


class Shield : public Weapon
{
public:

	Shield(int pp, string name) : Weapon(0, pp, name)
	{
		cout << "Created shield, protection level: " << pp << endl;
	};

	int Strike()
	{
		cout << "There is no damage using shield" << endl;
		return 0;
	}
	int Protect(int damage)
	{
		int def = rnd(protect_power);
		protect_power = protect_power - def / 10;
		def = damage - def;
		def = (def > 0 ? def : 0);
		cout << "You took " << def << " damage" << endl;
		return def;
	}

	~Shield() {}

protected:

private:
};
