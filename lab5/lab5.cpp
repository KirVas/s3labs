#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include "Shield.h"
#include "Club.h"
#include "Sword.h"


using namespace std;

int main()
{
	Shield s = Shield(121, "Pan lid");
	s.Strike();
	s.Protect(59);

	Club c = Club(10, 5, "Stick");
	c.Protect(10);
	c.Strike();

	Sword sw = Sword(100, 25, "Shadow of Death");
	sw.Strike();
	sw.Protect(30);

//	system("pause");
	return 0;
}
