#pragma once

#include <math.h>
#include <string.h>

using namespace std;

class Weapon
{
public:

	//Weapon(int bp, int pp) : blow_power(bp), protect_power(pp) {};
	Weapon(int bp, int pp, string name) : blow_power(bp), protect_power(pp), name(name) {}

	virtual int Strike() = 0;
	virtual int Protect(int damage) = 0;

	string GetName()
	{
		return name;
	}

	int rnd(int border)
	{
		return rand() % (border / 2) + rand() % (border / 2);
	}


	~Weapon() {};

protected:
	string name;
	int blow_power;
	int protect_power;
};

