#include <iostream>

using namespace std;

struct Node
{
	char * name;
	Node * left;
	Node * right;
};

void AddNode(Node * root, char * name);
void DelTree(Node * root);
void PrintTree(Node * root);

int main()
{
	char * name = new char[10];
	cin >> name;
	Node * myTree = new Node;
	myTree->left = NULL;
	myTree->right = NULL;
	myTree->name = name;
	for (int i = 0; i < 3; i++)
	{
		char * elname = new char[10];
		cin >> elname;
		AddNode(myTree, elname);
	}
	PrintTree(myTree);
	DelTree(myTree);
	//system("pause");
    return 0;
}

void AddNode(Node * root, char * name)
{
	Node * toAdd = new Node;
	toAdd->left = NULL;
	toAdd->right = NULL;
	toAdd->name = name;
	while (root != NULL)
	{
		if (*name > *root->name)
		{
			if (root->right != NULL) root = root->right;
			else
			{
				root->right = toAdd;
				return;
			}
		}
		else
		{
			if (root->left != NULL) root = root->left;
			else
			{
				root->left = toAdd;
				return;
			}
		}
	}
	return;
}

void DelTree(Node * root)
{
	if (root == NULL) return;
	DelTree(root->left);
	DelTree(root->right);
	delete[] (root->name);
	delete root;
	root = NULL;
	return;
}

void PrintTree(Node * root)
{
	if (root == NULL) return;
	PrintTree(root->left);
	cout << root->name << endl;
	PrintTree(root->right);
	return;
}