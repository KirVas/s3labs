#define _CRT_SECURE_NO_WARNINGS 

#include <iostream>
#include <cstring>

using namespace std;

class Node
{
	char * name = NULL;
	Node * left;
	Node * right;


public:

	Node(char * name, Node * left, Node * right) : name(name), left(left), right(right) {}
	Node(char * name) : name(name), left(NULL), right(NULL) {}
	Node()
	{
		name = "defaultname";
		left = NULL;
		right = NULL;
	}
	Node(const Node& n)
	{
		char * myName = new char[50];
		myName = strcpy(myName, n.name);
		left = n.left;
		right = n.right;
	}
	~Node()
	{
		if (left != NULL) left->DelTree();
		if (right != NULL) right->DelTree();
		delete[] name;
	}

	Node& operator=(const Node& n)
	{
		if (this != &n)
		{
			delete[] name;
			name = new char[50];
			name = strcpy(name, n.name);
		}
	}

	Node& operator<<(char* myName)
	{
		Node * toAdd = new Node(myName);
		left = toAdd;
		return *this;
	}
	Node& operator>(char* myName)
	{
		Node * toAdd = new Node(myName);
		right = toAdd;
		return *this;
	}
	Node& operator[](const char* myName)
	{
		Node * search = this;
		while (true)
		{
			if (strcmp(search->name, myName) == 0) return *search;
			if (strcmp(search->name, myName) > 0)
			{
				if (search->left != NULL) search = search->left; else return *(new Node("NULL", NULL, NULL));
			}
			else
			{
				if (search->right != NULL) search = search->right; else return *(new Node("NULL", NULL, NULL));
			}
		}

	}

	friend ostream &operator<<(ostream &ost, const Node &node)
	{
		return ost << node.myname();
	}

	const char * myname() const
	{
		return this->name;
	}

	void Init(char * myName)
	{
		left = NULL;
		right = NULL;
		name = myName;
	}
	void AddNode(char * name)
	{
		Node * search = this;
		Node *toAdd = new Node(name, NULL, NULL);
		while (true)
		{
			if (strcmp(search->name, name) > 0)
			{
				if (search->left != NULL) search = search->left;
				else
				{
					search->left = toAdd;
					return;
				}
			}
			else
			{
				if (search->right != NULL) search = search->right;
				else
				{
					search->right = toAdd;
					return;
				}
			}
		}
		return;
	}
	void DelTree()
	{
		delete this;
		return;
	}

	void PrintTree()
	{
		if (left != NULL) left->PrintTree();
		cout << name << endl;
		if (right != NULL) right->PrintTree();
		return;
	}
	char * GetName()
	{
		return name;
	}
};



int main()
{
	char * name = new char[50];
	cin >> name;
	Node * myTree = new Node(name, NULL, NULL);
	char * nameLeft = new char[50];
	char * nameRight = new char[50];
	cin >> nameLeft;
	cin >> nameRight;
	*(myTree) << nameLeft;
	*(myTree) > nameRight;

	for (int i = 0; i < 3; i++)
	{
		char * elname = new char[50];
		cin >> elname;
		myTree->AddNode(elname);
	}
	char * toFind = new char[50];
	cout << "================================" << endl;
	cin >> toFind;
	cout << (*myTree)[toFind] << endl;
	delete[] toFind;
	cout << "================================" << endl;
	myTree->PrintTree();
	myTree->DelTree();
	//system("pause");
	return 0;
}
