#include <iostream>

using namespace std;

template <class T> struct ListItem
{
	T data;
	ListItem<T> * next;
};
template <class T> class List
{

private:

	ListItem<T> * list;

public:

	List<T>()
	{
		list = new ListItem<T>;
		list->next = NULL;
	}

	~List<T>()
	{
		ListItem<T> * next;
		ListItem<T> * toDel = list;
		while (toDel != NULL)
		{
			next = toDel->next;
			delete toDel;
			toDel = next;
		}

	}

	void AddElement(T element)
	{
		ListItem<T> * search = list;
		while (search->next != NULL) search = search->next;
		search->data = element;
		search->next = new ListItem<T>;
		search->next->next = NULL;
	}

	void PrintList()
	{
		ListItem<T> * search = list;
		while (search->next != NULL)
		{
			cout << (search->data) << " ";
			search = search->next;
		}
	}

	void DeleteElement(T value)
	{

		if (list->data == value)
		{
			ListItem<T> * search = list->next;
			delete list;
			list = search;
			return;
		}

		ListItem<T> * search = list;
		ListItem<T> * toDel = NULL;
		while (search->next->data != value && search->next != NULL) search = search->next;
		if (search->next->data == value)
		{
			toDel = search->next;
			search->next = search->next->next;
			delete toDel;
		}
		else
		{
			cout << "nothing to del" << endl;
		}
	}

};

int main()
{
	List<int> * myList = new List<int>();
	myList->AddElement(2);
	myList->AddElement(6);
	myList->AddElement(7);
	myList->PrintList();
	cout << endl << "====================" << endl;
	List<char> * myCharList = new List<char>();
	myCharList->AddElement('a');
	myCharList->AddElement('z');
	myCharList->AddElement('s');
	myCharList->PrintList();


	myList->DeleteElement(2);
	myList->PrintList();
	myCharList->DeleteElement('s');
	myCharList->PrintList();

	delete myCharList;
	delete myList;
	cout << endl;
	//system("pause");
	return 0;
}




